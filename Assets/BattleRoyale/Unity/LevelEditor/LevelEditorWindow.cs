using System;
using System.Collections.Generic;
using System.IO;
using BattleRoyale.Core.Data.Level;
using BattleRoyale.Core.JsonManager;
using Newtonsoft.Json;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace BattleRoyale.Unity.LevelEditor
{
    public class LevelEditorWindow : EditorWindow
    {
        private const string FORMATTING_KEY = nameof(LevelEditorWindow) + "." + nameof(JsonConvert) + "." + nameof(Formatting);

        private static Vector2 _scroll;

        private static int _size;
        private static LevelMap _map;
        private static string _json;

        private static Dictionary<CellType, Color> _cellButtonColors;
        private static LevelView.LevelView levelView;

        private static readonly int SINGLE_LINE_HEIGHT = Mathf.RoundToInt(EditorGUIUtility.singleLineHeight);
        private static int CellSize
        {
            get => EditorPrefs.GetInt(nameof(LevelEditorWindow) + "." + nameof(CellSize), 27);
            set => EditorPrefs.SetInt(nameof(LevelEditorWindow) + "." + nameof(CellSize), value);
        }

        private static Formatting Formatting
        {
            get => EditorPrefs.GetBool(FORMATTING_KEY, true) ? Formatting.Indented : Formatting.None;
            set => EditorPrefs.SetBool(FORMATTING_KEY, value == Formatting.Indented);
        }


        [MenuItem("BattleRoyale/" + nameof(LevelEditorWindow) + " %#&e")]
        private static void Open()
        {
            GetWindow<LevelEditorWindow>();
        }


        private void Awake()
        {
            titleContent = new GUIContent(nameof(LevelEditor));
            Reload(false);
        }

        private void OnFocus()
        {
            Reload(false);
        }


        private static void Reload(bool quick = true)
        {
            (_map, _json) = JsonManager.Load<LevelMap>();
            _size = _map.Map.Length;
            if (quick)
                return;

            _cellButtonColors = new Dictionary<CellType, Color>();
            foreach (CellType cellType in (CellType[])Enum.GetValues(typeof(CellType)))
            {
                Color color = CellsConfigSO.Instance[cellType].Prefab.GetComponent<MeshRenderer>().sharedMaterial.color;
                _cellButtonColors.Add(cellType, color);
            }
        }

        private void OnGUI()
        {
            _scroll = EditorGUILayout.BeginScrollView(_scroll);

            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.LabelField("Level size", GUILayout.Width(60));
                _size = EditorGUILayout.IntSlider(_size, LevelMap.MIN_SIZE, LevelMap.MAX_SIZE);
                if (GUILayout.Button("Reset"))
                    _size = _map.Map.Length;
                if (GUILayout.Button("Apply"))
                {
                    _map = new LevelMap(_size, _map);
                    Apply(() => Reload());
                }
            }
            EditorGUILayout.EndHorizontal();

            switch (Formatting)
            {
                case Formatting.None:
                    if (GUILayout.Button("Save Intended"))
                        Formatting = Formatting.Indented;
                    break;

                case Formatting.Indented:
                    if (GUILayout.Button("Save Ugly"))
                        Formatting = Formatting.None;
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }

            var cellTypes = new List<CellType>((CellType[])Enum.GetValues(typeof(CellType)));
            Color color = GUI.color;

            EditorGUILayout.BeginHorizontal();
            {
                CellSize = EditorGUILayout.IntSlider(CellSize, SINGLE_LINE_HEIGHT, SINGLE_LINE_HEIGHT * 3);
            }
            EditorGUILayout.EndHorizontal();

            GUILayoutOption[] options =
            {
                GUILayout.Width(CellSize),
                GUILayout.Height(CellSize),
            };
            var style = new GUIStyle(GUI.skin.window);
            style.alignment = TextAnchor.MiddleCenter;
            EditorGUILayout.BeginVertical();
            {
                for (int y = _map.Map.Length - 1; y >= 0; y--)
                {
                    EditorGUILayout.BeginHorizontal();
                    {
                        for (var x = 0; x < _map.Map[y].Cells.Length; x++)
                        {
                            CellType cell = _map.Map[y].Cells[x];
                            var spawn = false;
                            foreach (Vector2Int spawnPoint in _map.SpawnPoints)
                            {
                                if (spawnPoint == new Vector2Int(x, y))
                                {
                                    spawn = true;
                                    break;
                                }
                            }

                            GUI.color = _cellButtonColors[cell];
                            if (spawn)
                            {
                                GUILayout.Box("X", style, options);
                                continue;
                            }

                            if (GUILayout.Button("", style, options))
                            {
                                int index = cellTypes.IndexOf(cell);
                                index++;
                                if (index >= cellTypes.Count)
                                    index = 0;
                                _map.Map[y].Cells[x] = cellTypes[index];
                                Apply();
                            }
                        }
                    }
                    EditorGUILayout.EndHorizontal();
                }
            }
            EditorGUILayout.EndVertical();
            GUI.color = color;


            EditorGUILayout.TextArea(_json);

            EditorGUILayout.EndScrollView();
        }

        private static void Apply(Action afterSaveBeforeApply = null)
        {
            JsonManager.Save(_map, Formatting);
            afterSaveBeforeApply?.Invoke();
            if (levelView == null)
            {
                EditorSceneManager.OpenScene(Path.Combine("Assets", "Scenes", "Gameplay.unity"));
                levelView = FindObjectOfType<LevelView.LevelView>();
            }
            if (levelView != null)
                levelView.Spawn(_map);
        }
    }
}