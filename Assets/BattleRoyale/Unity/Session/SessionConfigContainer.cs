using BattleRoyale.Core.Data;
using BattleRoyale.Core.Data.Level;
using BattleRoyale.Core.Data.Player;
using BattleRoyale.Core.JsonManager;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BattleRoyale.Unity.Session
{
    public class SessionConfigContainer : MonoBehaviour
    {
        [ShowInInspector, ReadOnly]
        // [NonSerialized] public SessionConfig SessionConfig;
        private SessionConfig _sessionConfig;

        public SessionConfig SessionConfig
        {
            get
            {
                if (_sessionConfig == null)
                    Load();
                return _sessionConfig;
            }
        }


        [Button]
        private void Load()
        {
            (SessionConfig.Map, _) = JsonManager.Load<LevelMap>();
            (SessionConfig.CharacterBase, _) = JsonManager.Load<CharacterConfig>();
            (SessionConfig.Players, _) = JsonManager.Load<PlayersConfig>();
        }
    }
}