using System;
using BattleRoyale.Core.Data.Player;
using BattleRoyale.Core.Data.Weapon;
using BattleRoyale.Core.JsonManager;
using UnityEditor;
using UnityEngine;

namespace BattleRoyale.Unity.CharacterEditor
{
    public class CharacterEditorWindow : EditorWindow
    {
        private static Vector2 _scroll;

        private static CharacterConfig _character;
        private static string _characterJson;

        private static PlayersConfig _players;
        private static string _playersJson;


        [MenuItem("BattleRoyale/" + nameof(CharacterEditorWindow) + " %#&c")]
        private static void Open()
        {
            GetWindow<CharacterEditorWindow>();
        }


        private void Awake()
        {
            titleContent = new GUIContent(nameof(CharacterEditor));
            Reload();
        }

        private void OnFocus()
        {
            Reload();
        }


        private static void Reload()
        {
            while (true)
            {
                (_character, _characterJson) = JsonManager.Load<CharacterConfig>();
                if (_character == null)
                {
                    _character = new CharacterConfig();
                    JsonManager.Save(_character);
                    continue;
                }
                break;
            }

            while (true)
            {
                (_players, _playersJson) = JsonManager.Load<PlayersConfig>();
                if (_players == null)
                {
                    _players = new PlayersConfig(null);
                    JsonManager.Save(_players);
                    continue;
                }
                break;
            }
        }


        private void OnGUI()
        {
            _scroll = EditorGUILayout.BeginScrollView(_scroll);

            EditorGUIUtility.labelWidth = 50;
            int maxHp = EditorGUILayout.IntSlider(nameof(_character.MaxHp), _character.MaxHp, 1, 1000);
            float speed = EditorGUILayout.Slider(nameof(_character.Speed), _character.Speed, 0.01f, 10f);

            if (_character.MaxHp != maxHp || Math.Abs(_character.Speed - speed) > 0.001f)
            {
                _character.MaxHp = maxHp;
                _character.Speed = speed;
                JsonManager.Save(_character);
            }

            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Players");
            EditorGUIUtility.labelWidth = 105;
            for (var i = 0; i < _players.Players.Count; i++)
            {
                EditorGUILayout.Space();
                if (GUILayout.Button("+"))
                    _players.Players.Insert(i, new PlayerConfig(null));
                EditorGUILayout.Space();

                PlayerConfig player = _players.Players[i];
                string playerName = EditorGUILayout.TextField(nameof(player.Name), player.Name);

                EditorGUILayout.LabelField(nameof(player.Weapon));
                EditorGUI.indentLevel++;
                WeaponConfig weapon = player.Weapon;
                float wpnReloadTime = EditorGUILayout.Slider(nameof(weapon.ReloadTime), weapon.ReloadTime, 0.01f, 60f);

                EditorGUILayout.LabelField(nameof(player.Weapon.Projectile));
                EditorGUI.indentLevel++;
                Projectile p = weapon.Projectile;
                var projectileType = (ProjectileType)EditorGUILayout.EnumPopup(nameof(Type), p.ProjectileType);
                float prjSpreadAngle = EditorGUILayout.Slider(nameof(p.SpreadAngle), p.SpreadAngle, 0.01f, 180f);
                float prjSpeed = EditorGUILayout.Slider(nameof(p.Speed), p.Speed, 0.01f, 10f);
                int prjDamage = EditorGUILayout.IntSlider(nameof(p.Damage), p.Damage, 1, 1000);

                EditorGUI.indentLevel -= 2;

                if (playerName != player.Name || Math.Abs(
                    wpnReloadTime - player.Weapon.ReloadTime) > 0.001f ||
                    projectileType != player.Weapon.Projectile.ProjectileType || Math.Abs(
                    prjSpreadAngle - player.Weapon.Projectile.SpreadAngle) > 0.001f || Math.Abs(
                    prjSpeed - player.Weapon.Projectile.Speed) > 0.001f ||
                    prjDamage != player.Weapon.Projectile.Damage)
                {
                    player.Name = playerName;
                    player.Weapon.ReloadTime = wpnReloadTime;
                    player.Weapon.Projectile.ProjectileType = projectileType;
                    player.Weapon.Projectile.SpreadAngle = prjSpreadAngle;
                    player.Weapon.Projectile.Speed = prjSpeed;
                    player.Weapon.Projectile.Damage = prjDamage;
                    JsonManager.Save(_players);
                }
            }
            EditorGUILayout.Space();
            if (GUILayout.Button("+"))
                _players.Players.Add(new PlayerConfig(null));
            EditorGUILayout.Space();
            EditorGUILayout.Space();

            EditorGUILayout.TextArea(_characterJson);
            EditorGUILayout.TextArea(_playersJson);

            EditorGUILayout.EndScrollView();
        }
    }
}