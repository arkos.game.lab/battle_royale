using System;
using System.Collections.Generic;
using BattleRoyale.Core.Data.Level;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BattleRoyale.Unity
{
    [CreateAssetMenu(fileName = nameof(CellsConfigSO), menuName = "BattleRoyale/" + nameof(CellsConfigSO))]
    public class CellsConfigSO : ScriptableObject
    {
        [DisableInPlayMode]
        [SerializeField] private CellData[] _cells;

        [ShowInInspector, ReadOnly, HideInEditorMode]
        private Dictionary<CellType, CellViewData> _cellDic;

        private static CellsConfigSO _instance;
        public static CellsConfigSO Instance
        {
            get
            {
                if (_instance == null)
                    _instance = Resources.Load<CellsConfigSO>(nameof(CellsConfigSO));
                return _instance;
            }
        }

        public CellViewData this[CellType cellType]
        {
            get
            {
                if (_cellDic == null)
                {
                    _cellDic = new Dictionary<CellType, CellViewData>();
                    foreach (CellData cellData in _cells)
                    {
                        _cellDic.Add(cellData.CellType, cellData);
                    }
                }

                if (!_cellDic.TryGetValue(cellType, out CellViewData view))
                {
                    Debug.LogError($"{name} missing {nameof(CellType)}.{cellType}");
                    return null;
                }

                return view;
            }
        }
    }

    [Serializable]
    public class CellData : CellViewData
    {
        [DisableInPlayMode] public CellType CellType;
    }

    public class CellViewData
    {
        [Required(InfoMessageType.Error)]
        [DisableInPlayMode] public GameObject Prefab;
    }
}