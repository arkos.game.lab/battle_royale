using BattleRoyale.Core.Data.Level;
using BattleRoyale.Unity.Session;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BattleRoyale.Unity.LevelView
{
    public class LevelView : MonoBehaviour
    {
        [SerializeField, Required] private SessionConfigContainer _sessionConfigContainer;


        private void Start()
        {
            Spawn();
        }


#if UNITY_EDITOR
        [Button, PropertyOrder(-1), HorizontalGroup, DisableInPlayMode]
        public void Spawn() => Spawn(null);
        public
#else // !UNITY_EDITOR
        private
#endif // !UNITY_EDITOR
                void Spawn(LevelMap map = null)
        {
            Clear();

            map ??= _sessionConfigContainer.SessionConfig.Map;

            int size = map.Size;
            for (int y = -1; y <= size; y++)
            {
                Row row = y != -1 && y != size ? map.Map[y] : null;
                for (int x = -1; x <= size; x++)
                {
                    if (row == null || x == -1 || x == size)
                    {
                        SpawnCell(CellType.Wall__, x, y);
                        continue;
                    }

                    CellType cellType = row.Cells[x];
                    SpawnCell(cellType, x, y);
                }
            }

            void SpawnCell(CellType cellType, int x, int y)
            {
                Vector2 pos = new Vector2(x, y) - Vector2.one * size / 2f;
                SpawnCell2(cellType, pos);
                if (cellType == CellType.Bushes)
                    SpawnCell2(CellType.Empty_, pos);
            }

            void SpawnCell2(CellType cellType, Vector2 pos)
            {
                GameObject prefab = CellsConfigSO.Instance[cellType].Prefab;
                if (prefab == null)
                    return;

                GameObject cell = Instantiate(prefab, transform);
                cell.transform.position = new Vector3(pos.x, 0, pos.y);
            }
        }

#if UNITY_EDITOR
        [Button, PropertyOrder(-1), HorizontalGroup, DisableInPlayMode]
#endif // UNITY_EDITOR
        private void Clear()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                while (transform.childCount > 0)
                {
                    DestroyImmediate(transform.GetChild(0).gameObject);
                }
            }
            else
#endif // UNITY_EDITOR
            {
                foreach (Transform child in transform)
                {
                    Destroy(child.gameObject);
                }
            }
        }
    }
}