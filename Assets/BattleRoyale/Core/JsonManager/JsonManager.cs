using System.IO;
using BattleRoyale.Core.Data.Level;
using BattleRoyale.Core.Data.Player;
using Newtonsoft.Json;
using UnityEngine;

#if UNITY_ANDROID || UNITY_WEBGL
using UnityEngine.Networking;
#endif // UNITY_ANDROID || UNITY_WEBGL

namespace BattleRoyale.Core.JsonManager
{
    public static class JsonManager
    {
        private static readonly string PATH_ROOT = Application.streamingAssetsPath;
        private const string FILENAME_LEVEL = "level.json";
        private const string FILENAME_CHARACTER = "character.json";
        private const string FILENAME_PLAYERS = "players.json";

        public static (TData, string json) Load<TData>()
            where TData : class
        {
            string filename = GetFilename<TData>();
            if (filename == null)
            {
                Debug.LogError($"{nameof(MissingReferenceException)}: {typeof(TData).Name}");
                return (null, null);
            }

            string path = Path.Combine(PATH_ROOT, filename);
            string json = GetJson(path, filename);

            TData data;
            try
            {
                data = JsonConvert.DeserializeObject<TData>(json);
            }
            catch
            {
                Debug.LogError($"{nameof(JsonSerializationException)}: {filename}");
                return (null, null);
            }
            return (data, json);
        }

        private static string GetJson(string path, string filename)
        {
#if UNITY_ANDROID || UNITY_WEBGL
            UnityWebRequest request = UnityWebRequest.Get(path);
            UnityWebRequestAsyncOperation operation = request.SendWebRequest();
            // TODO await
            while (!operation.isDone) ;
            if (!string.IsNullOrWhiteSpace(request.error))
            {
                Debug.LogError($"{filename}: {request.error}");
                return null;
            }

            return request.downloadHandler.text;
#else // !UNITY_ANDROID && !UNITY_WEBGL
            if (!File.Exists(path))
            {
                Debug.LogError($"{nameof(FileNotFoundException)}: {filename}");
                return null;
            }

            return File.ReadAllText(path);
#endif // !UNITY_ANDROID && !UNITY_WEBGL
        }


        public static void Save<TData>(TData data, Formatting formatting = Formatting.Indented)
            where TData : class
        {
            string json = JsonConvert.SerializeObject(data, formatting);
            Save<TData>(json);
        }

        private static void Save<TData>(string json)
            where TData : class
        {
            if (!Directory.Exists(PATH_ROOT))
                Directory.CreateDirectory(PATH_ROOT);

            string filename = GetFilename<TData>();
            string path = Path.Combine(PATH_ROOT, filename);
            File.WriteAllText(path, json);
        }


        private static string GetFilename<TData>()
        {
            return typeof(TData).Name switch
            {
                nameof(LevelMap) => FILENAME_LEVEL,
                nameof(CharacterConfig) => FILENAME_CHARACTER,
                nameof(PlayersConfig) => FILENAME_PLAYERS,
                _ => throw new MissingReferenceException()
            };
        }
    }
}