using System;

namespace BattleRoyale.Core.Data.Level
{
    [Flags]
    public enum CellProperty
    {
// ReSharper disable InconsistentNaming
        Impenetrable = 1 << 0,
        Impassable__ = 1 << 1,
        Hiddable____ = 1 << 2,
// ReSharper restore InconsistentNaming
    }
}