using System;
using UnityEngine;

namespace BattleRoyale.Core.Data.Level
{
    [Serializable]
    public class LevelMap
    {
        public const int MIN_SIZE = 3;
        public const int MAX_SIZE = 20;

        public Row[] Map;
        public Vector2Int[] SpawnPoints;

        public int Size => Map.Length;

        public LevelMap()
        {
        }

        public LevelMap(int size)
        {
            switch (size)
            {
                case < MIN_SIZE: Debug.LogError($"Level is too small"); break;
                case > MAX_SIZE: Debug.LogError($"Level is too big"); break;
            }

            Map = new Row[size];
            for (var i = 0; i < size; i++)
            {
                Map[i] = new Row
                {
                    Cells = new CellType[size],
                };
            }

            SpawnPoints = new[] {new Vector2Int(size / 2, 0), new Vector2Int(size / 2 + (1 - size % 2), size - 1)};
            foreach (Vector2Int spawnPoint in SpawnPoints)
            {
                Map[spawnPoint.x].Cells[spawnPoint.y] = CellType.Empty_;
            }
        }

        public LevelMap(int size, LevelMap source)
            : this(size)
        {
            for (var y = 0; y < Mathf.Min(size, source.Map.Length); y++)
            {
                for (var x = 0; x < Mathf.Min(size, source.Map[y].Cells.Length); x++)
                {
                    Map[y].Cells[x] = source.Map[y].Cells[x];
                }
            }
        }
    }

    // for Unity Inspector
    [Serializable]
    public class Row
    {
        public CellType[] Cells;
    }
}