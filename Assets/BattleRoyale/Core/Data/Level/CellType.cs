namespace BattleRoyale.Core.Data.Level
{
    public enum CellType
    {
// ReSharper disable InconsistentNaming
        Empty_ = 0,
        Wall__ = CellProperty.Impenetrable | CellProperty.Impassable__,
        Water_ = CellProperty.Impassable__,
        Bushes = CellProperty.Hiddable____,
// ReSharper restore InconsistentNaming
    }
}