using System;

namespace BattleRoyale.Core.Data.Weapon
{
    [Serializable]
    public class Projectile
    {
        public ProjectileType ProjectileType;
        public float SpreadAngle;
        public float Speed;
        public int Damage;

        public Projectile(Projectile source)
        {
            ProjectileType = source?.ProjectileType ?? default;
            SpreadAngle = source?.SpreadAngle ?? default;
            Speed = source?.Speed ?? default;
            Damage = source?.Damage ?? default;
        }
    }
}