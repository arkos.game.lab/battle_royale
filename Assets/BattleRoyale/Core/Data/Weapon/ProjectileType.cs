namespace BattleRoyale.Core.Data.Weapon
{
    public enum ProjectileType
    {
        Bullet,
        Mortar,
    }
}