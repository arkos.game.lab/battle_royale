using System;

namespace BattleRoyale.Core.Data.Weapon
{
    [Serializable]
    public class WeaponConfig
    {
        public float ReloadTime;
        public Projectile Projectile = new(null);

        public WeaponConfig(WeaponConfig source)
        {
            ReloadTime = source?.ReloadTime ?? default;
            Projectile = new Projectile(source?.Projectile);
        }
    }
}