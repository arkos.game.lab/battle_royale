using System;
using System.Collections.Generic;

namespace BattleRoyale.Core.Data.Player
{
    [Serializable]
    public class PlayersConfig
    {
        public List<PlayerConfig> Players = new();

        public PlayersConfig(PlayersConfig source)
        {
            int count = source?.Players?.Count ?? 0;
            if (count <= 0)
                return;

            foreach (PlayerConfig player in source.Players)
            {
                Players.Add(new PlayerConfig(player));
            }
        }
    }
}