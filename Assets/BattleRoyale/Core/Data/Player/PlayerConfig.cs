using System;
using BattleRoyale.Core.Data.Weapon;

namespace BattleRoyale.Core.Data.Player
{
    [Serializable]
    public class PlayerConfig
    {
        public string Name;
        public WeaponConfig Weapon = new(null);

        public PlayerConfig(PlayerConfig source)
        {
            Name = source?.Name ?? "Give_me_my_Name_Plz";
            Weapon = new WeaponConfig(source?.Weapon);
        }
    }
}