using System;

namespace BattleRoyale.Core.Data.Player
{
    [Serializable]
    public class CharacterConfig
    {
        public int MaxHp;
        public float Speed;
    }
}