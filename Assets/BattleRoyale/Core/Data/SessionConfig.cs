using System;
using BattleRoyale.Core.Data.Level;
using BattleRoyale.Core.Data.Player;

namespace BattleRoyale.Core.Data
{
    [Serializable]
    public class SessionConfig
    {
        public LevelMap Map;
        public CharacterConfig CharacterBase;
        public PlayersConfig Players;
    }
}